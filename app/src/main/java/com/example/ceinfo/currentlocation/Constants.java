package com.example.ceinfo.currentlocation;

import android.content.Context;

/**
 * Created by CEINFO on 27-05-2016.
 */
public class Constants {

    public static final int mId = 0;
    //TAGS
    public static final String TAG = "CUSTOM_TAG";
    public static final String IMPORTANT = "IMPORTANT";
    public static final String ANOTHER = "ANOTHER";

    public static final int REQUEST_ACCESS_FINE_LOCATION = 0;

    public static final String BROADCAST_ACTION = "BROADCAST_ACTION";

    public static final String SERVICE_STATUS = "SERVICE_STATUS";
    public static final String PREFS = "PREFS";

    public static final String START_SERVICE = "START_SERVICE";
    public static final String STOP_SERVICE = "STOP_SERVICE";
    public static final String MAIN_ACTION = "MAIN_ACTION";

    public static final int FOREGROUND_SERVICE = 101;
    public static final String MESSAGE = "MESSAGE";
    public static final String base_url = "http://192.168.0.55/pao/";

    public static final int PERMISSION_REQUEST = 0;
    public static final int LOCATION_REQUEST = 1;
    public static final int EXIT_REQUEST = 2;

    public static final String USERNAME = "USERNAME";

    public static final String UPDATE_BOADCAST = "UPDATE_BROADCAST";
    public static final String UPDATE_MESSAGE = "UPDATE_MESSAGE";

    public static final int SERVICE_START = 3;
    public static final int SERVICE_END = 4;
    public static final int FIX_DELAY = 5;
    public static final String UPDATE_DELAY = "UPDATE_DELAY";
    public static final int TOAST = 10;
    public static final String DISPLAY_ALERT = "DISPLAY_ALERT";
}
