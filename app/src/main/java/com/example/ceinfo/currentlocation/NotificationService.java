package com.example.ceinfo.currentlocation;

import android.app.*;
import android.app.AlarmManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.os.SystemClock;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.ActivityRecognition;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by CEINFO on 27-05-2016.
 * //TODO location settings result to be implemented
 */
public class NotificationService extends Service
        implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener, SensorEventListener{

    /**
     * checked if client connected for
     * --request location updates
     * --disconnect
     * --remove location updates
     */

    private NotificationCompat.Builder mBuilder;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private DBHelper myDB;
    private UpdateReceiver updateReceiver;
    boolean mRequestingLocationUpdates = false;
    private GPSTracker gps;
    private android.app.AlarmManager alarmManager;
    private PendingIntent pendingIntent;
    private int delay = 3000;
    private boolean playServicesAvailable = false;
    SensorManager mSensorManager;
    Sensor mSensor;
    private long last_update = 0;
    private double last_x, last_y, last_z;
    private static final int STILL_THRESHOLD = 10;
    private static final int WALKING_THRESHOLD = 400;
    private static final int RUNNING_THRESHOLD = 800;
    Location mLastLocation;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * initialised APIClient, LocationRequest
     * regularly check if settings available
     *
     */
    @Override
    public void onCreate() {
        super.onCreate();

        if(isGooglePlayServicesAvailable(this)) {

            Log.i(Constants.TAG, "API services available");

            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .addApi(ActivityRecognition.API)
                    .build();

            mLocationRequest = new LocationRequest()
                    .setInterval(10000)
                    .setFastestInterval(10000)
                    .setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

            playServicesAvailable = true;
        }else{

            Log.i(Constants.TAG, "API services unavailable");

            gps = new GPSTracker(this);
            playServicesAvailable = false;

            mSensorManager = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
            mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            mSensorManager.registerListener(this, mSensor, 30*1000*1000);
        }

        updateReceiver = new UpdateReceiver();

        alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        Intent another = new Intent(this, AlarmReceiver.class);
        pendingIntent = PendingIntent.getBroadcast(this, 0, another, 0);

        alarmManager.set(
                AlarmManager.ELAPSED_REALTIME,
                SystemClock.elapsedRealtime()+delay,
                pendingIntent
        );

        /*(new Thread(new Runnable() {
            @Override
            public void run() {
                int count = 0;
                while (true) {
                    try {
                        if (run) {
                            count = 0;

                            if (ContextCompat.checkSelfPermission(NotificationService.this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                                    != PackageManager.PERMISSION_GRANTED) {
                                Intent intent = new Intent(Constants.BROADCAST_ACTION);
                                intent.putExtra(Constants.MESSAGE, Constants.PERMISSION_REQUEST);
                                sendBroadcast(intent);
                            }

                            if (Settings.Secure.getInt(NotificationService.this.getContentResolver(), Settings.Secure.LOCATION_MODE) != 3) {
                                Intent other = new Intent(Constants.BROADCAST_ACTION);
                                other.putExtra(Constants.MESSAGE, Constants.LOCATION_REQUEST);
                                sendBroadcast(other);
                                run = false;
                            }
                        } else {
                            if (count < 50) {
                                count++;
                            }
                            if (count == 1) {
                                //TODO
//                                mBuilder.setContentText("Location settings changed");
//                                startForeground(Constants.FOREGROUND_SERVICE, mBuilder.build());
                            }
                        }
                    } catch (Settings.SettingNotFoundException e) {
                        Intent other = new Intent(Constants.BROADCAST_ACTION);
                        other.putExtra(Constants.MESSAGE, Constants.EXIT_REQUEST);
                        sendBroadcast(other);
                        exit();
                    }

                    try {
                        Thread.sleep(30000);
                    } catch (InterruptedException e) {
                        //Thread never interrupted
                        e.printStackTrace();
                    }
                }
            }
        })).start();*/

    }

    public boolean isGooglePlayServicesAvailable(Context activity) {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int status = googleApiAvailability.isGooglePlayServicesAvailable(activity);
        if(status != ConnectionResult.SUCCESS) {

            return false;
        }
        return false;
    }
    /**
     * set the shared preferences boolean based on whether service is starting or not
     *
     * if start called-->
     * connect with API client
     * go to main activity on clicking notification
     * start a foreground service
     *
     * if stop called -->
     * exit
     *
     * @param intent
     * @param flags
     * @param startId
     * @return
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (intent == null || intent.getAction().equals(Constants.START_SERVICE)) {

            myDB = new DBHelper(this, intent.getStringExtra(Constants.USERNAME));

            SharedPreferences.Editor editor = getSharedPreferences(Constants.PREFS, MODE_PRIVATE).edit();
            editor.putBoolean(Constants.SERVICE_STATUS, true);
            editor.commit();


            IntentFilter intentFilter = new IntentFilter(Constants.UPDATE_BOADCAST);
            registerReceiver(updateReceiver, intentFilter);

            Log.i(Constants.TAG, "MainActivity set as click to go");
            Intent notificationIntent = new Intent(this, MainActivity.class);
            notificationIntent.setAction(Constants.MAIN_ACTION);
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                    | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                    notificationIntent, 0);

            mBuilder = new NotificationCompat.Builder(this)
                    .setContentTitle("Current Location")
                    .setTicker("Current Location")
                    .setContentText("your location")
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentIntent(pendingIntent)
                    .setOngoing(true);

            startForeground(Constants.FOREGROUND_SERVICE, mBuilder.build());

            if(playServicesAvailable){
                if (mGoogleApiClient.isConnected()) {
                    //TODO when connected take from on connect
                    startLocationUpdates();
                    startActivityUpdates();
                } else {
                    mGoogleApiClient.connect();
                }
            }else{
                mLastLocation = gps.getLocation();
            }

            if(mLastLocation!=null){
                myDB.addLocation(mLastLocation);
                sendToDb();
                mBuilder.setContentText("Co-ordinates:" + mLastLocation.getLatitude() + "," + mLastLocation.getLongitude());
                startForeground(Constants.FOREGROUND_SERVICE, mBuilder.build());
            }
        }else if (intent.getAction().equals(Constants.STOP_SERVICE)) {

            SharedPreferences.Editor editor = getSharedPreferences(Constants.PREFS, MODE_PRIVATE).edit();
            editor.putBoolean(Constants.SERVICE_STATUS, false);
            editor.commit();

            try{
                unregisterReceiver(updateReceiver);
            }catch (IllegalArgumentException e){
                //TODO already registered
            }

            if(playServicesAvailable){
                if (mGoogleApiClient.isConnected() && mRequestingLocationUpdates) {
                    LocationServices.FusedLocationApi.removeLocationUpdates(
                            mGoogleApiClient, this
                    ).setResultCallback(
                            new ResultCallback<Status>() {
                                @Override
                                public void onResult(@NonNull Status status) {
                                    mRequestingLocationUpdates = false;
                                    Log.d(Constants.TAG, "Removed location updates!");
                                }
                            }
                    );

                    ActivityRecognition.ActivityRecognitionApi.removeActivityUpdates(
                            mGoogleApiClient, pendingIntent
                    );
                }
            }else{
                if(playServicesAvailable){
                    gps.stopUsingGPS();
                }else{
                    mSensorManager.unregisterListener(this, mSensor);
                }
            }
            exit();
        }
        return START_STICKY;
    }

    /**
     * stop the service
     */
    public void exit() {
        if (playServicesAvailable && mGoogleApiClient.isConnected()){
            mGoogleApiClient.disconnect();
        }
        stopForeground(true);
        stopSelf();
    }

    /**
     * request location updates if permission present
     */
    public void startLocationUpdates() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            Intent other = new Intent(Constants.BROADCAST_ACTION);
            other.putExtra(Constants.MESSAGE, Constants.PERMISSION_REQUEST);
            sendBroadcast(other);
        } else {
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient, mLocationRequest, this
            ).setResultCallback(
                    new ResultCallback<Status>() {
                        @Override
                        public void onResult(@NonNull Status status) {
                            mRequestingLocationUpdates = true;
                            Log.d(Constants.TAG, "Started location updates!");
                        }
                    }
            );
        }
    }

    /**
     * callback method after connecting
     *
     * @param bundle
     */
    @Override
    public void onConnected(@Nullable Bundle bundle) {

        startActivityUpdates();

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED){
            Intent other = new Intent(Constants.BROADCAST_ACTION);
            other.putExtra(Constants.MESSAGE, Constants.PERMISSION_REQUEST);
            sendBroadcast(other);
        }else{
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if (mLastLocation!=null){
                updateLocation(mLastLocation);
            }else{
                //TODO in case its null
            }
        }

        startLocationUpdates();
//        Log.d(Constants.TAG, "Starting location updates...");
    }

    public void startActivityUpdates(){
        Intent intent = new Intent(this, ActivityRecognizedService.class );
        PendingIntent pendingIntent = PendingIntent.getService( this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT );
        ActivityRecognition.ActivityRecognitionApi.requestActivityUpdates( mGoogleApiClient, 30*1000, pendingIntent );
    }

    @Override
    public void onConnectionSuspended(int i) {
        //TODO avoid repeating re-connections
//        Log.d(Constants.TAG, "Connection suspended: "+i);
        if (mRequestingLocationUpdates){
            LocationServices.FusedLocationApi.removeLocationUpdates(
                    mGoogleApiClient, this
            ).setResultCallback(
                    new ResultCallback<Status>() {
                        @Override
                        public void onResult(@NonNull Status status) {
                            mRequestingLocationUpdates = false;
                            Log.d(Constants.TAG, "Removed location updates!");
                        }
                    }
            );
        }
        ActivityRecognition.ActivityRecognitionApi.removeActivityUpdates(mGoogleApiClient, pendingIntent);
        mBuilder.setContentTitle("Connection suspended");
        mBuilder.setContentText("Reconnecting in few moments...");
        startForeground(Constants.FOREGROUND_SERVICE, mBuilder.build());
        try {
            Thread.sleep(5*1000);
        } catch (InterruptedException e) {
            //Not reached
            e.printStackTrace();
        }
        mGoogleApiClient.connect();
    }

    @Override
    public void onLocationChanged(Location location) {
        updateLocation(location);
    }

    public void updateLocation(Location location){


        Log.d(Constants.TAG,"location updated..");
        mBuilder.setContentText("Co-ordinates:" + location.getLatitude() + "," + location.getLongitude());
        startForeground(Constants.FOREGROUND_SERVICE, mBuilder.build());
        myDB.addLocation(location);
        sendToDb();

        if (mGoogleApiClient.isConnected() && mRequestingLocationUpdates){

            LocationServices.FusedLocationApi.removeLocationUpdates(
                    mGoogleApiClient, this
            ).setResultCallback(
                    new ResultCallback<Status>() {
                        @Override
                        public void onResult(@NonNull Status status) {
                            mRequestingLocationUpdates = false;
                            Log.d(Constants.TAG, "Removed Location Updates...");
                        }
                    }
            );
            alarmManager.set(
                    AlarmManager.ELAPSED_REALTIME,
                    SystemClock.elapsedRealtime()+delay,
                    pendingIntent
            );
        }

        Toast.makeText(NotificationService.this, "Location Changed,"+delay, Toast.LENGTH_SHORT).show();
    }

    public void updateLocationUsingGPS(Location location) {
        mBuilder.setContentText("Co-ordinates:" + location.getLatitude() + "," + location.getLongitude());
        startForeground(Constants.FOREGROUND_SERVICE, mBuilder.build());
        myDB.addLocation(location);
        sendToDb();

        alarmManager.set(
                AlarmManager.ELAPSED_REALTIME,
                SystemClock.elapsedRealtime()+delay,
                pendingIntent
        );
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void sendToDb() {
        Log.d(Constants.TAG, "checking if network available...");
        boolean connected = isNetworkAvailable();
        if (connected) {
            Log.d(Constants.TAG, "Connected to internet...");
            ArrayList<String[]> locations = myDB.getAllLocations();
            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            for (int i = 0; i < locations.size(); i++) {
                final String[] details = locations.get(i);
                final int id = Integer.parseInt(details[8]);
                CustomRequest customRequest = new CustomRequest(
                        Request.Method.POST,
                        Constants.base_url + "location/add",
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Log.d(Constants.TAG, response);
                                myDB.updateStatus(id);
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                                //handle volley errors
                                if (error!=null){
                                    Log.w(Constants.TAG, error.getMessage());
                                }else{
                                    Log.w(Constants.TAG, "error came to be null");
                                }

                                if (error instanceof TimeoutError){
                                    Log.w(Constants.TAG, "Connection time out");
                                    Toast.makeText(NotificationService.this, "Taking too long to connect", Toast.LENGTH_SHORT).show();
                                }else if (error instanceof NoConnectionError){
                                    Log.w(Constants.TAG, "No Connection error");
                                    Toast.makeText(NotificationService.this, "Please check your internet connection", Toast.LENGTH_SHORT).show();
                                }else if (error instanceof AuthFailureError) {
                                    Log.w(Constants.TAG, "Authorization failure");
                                    Toast.makeText(NotificationService.this, "Authorization failed", Toast.LENGTH_SHORT).show();
                                } else if (error instanceof ServerError) {
                                    Log.e(Constants.TAG, "Server failure");
                                    Toast.makeText(NotificationService.this, "Server error occurred", Toast.LENGTH_SHORT).show();
                                } else if (error instanceof NetworkError) {
                                    Log.w(Constants.TAG, "Network failure");
                                    Toast.makeText(NotificationService.this, "Network error occurred", Toast.LENGTH_SHORT).show();
                                } else if (error instanceof ParseError) {
                                    Log.e(Constants.TAG, "Parse failure");
                                    Toast.makeText(NotificationService.this, "Parse error occurred", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }, NotificationService.this) {

                    @Override
                    protected Map<String, String> getParams() {
                        HashMap<String, String> params = new HashMap<>();
                        params.put("longitude", details[0]);
                        params.put("latitude", details[1]);
                        params.put("altitude", details[2]);
                        params.put("accuracy", details[3]);
                        params.put("bearing", details[4]);
                        params.put("speed", details[5]);
                        params.put("provider", details[6]);
                        params.put("time", details[7]);
                        return params;
                    }
                };
                requestQueue.add(customRequest);
            }
        } else {
            Log.d(Constants.TAG, "Not connected");
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(Constants.TAG, "Connection failed due to: "+connectionResult.getErrorMessage());
        /*if (mRequestingLocationUpdates){
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mRequestingLocationUpdates = false;
        }
        mBuilder.setContentTitle("Connection failed");
        mBuilder.setContentText("Reconnecting...");
        startForeground(Constants.FOREGROUND_SERVICE, mBuilder.build());
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            //Not reached
            e.printStackTrace();
        }
        mGoogleApiClient.connect();*/
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        Sensor mySensor = event.sensor;

        if (mySensor.getType() == Sensor.TYPE_ACCELEROMETER){
            double x = event.values[0];
            double y = event.values[1];
            double z = event.values[2];

            long current_time = System.currentTimeMillis();

            if (current_time-last_update>20*1000){
                long diff_time = current_time - last_update;
                last_update = current_time;

                double speed = Math.sqrt((x - last_x)*(x - last_x) + (y-last_y)*(y-last_y) + (z-last_z)*(z-last_z))/diff_time*10000;
                if (speed < STILL_THRESHOLD){
                    delay = 10*60*1000;
                }else if(speed < WALKING_THRESHOLD){
                    delay = 2*60*1000;
                }else if (speed < RUNNING_THRESHOLD){
                    delay = 45*1000;
                }else{
                    delay = 15*1000;
                }

                last_x = x;
                last_y = y;
                last_z = z;
            }
        }

        Toast.makeText(NotificationService.this, String.valueOf(delay), Toast.LENGTH_SHORT).show();
//        mSensorManager.unregisterListener(this, mSensor);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        //TODO on accuracy changed
    }

    //TODO check if location in high accuracy mode
    class UpdateReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(Constants.TAG, "Received from AlarmReceiver");
            int request = intent.getIntExtra(Constants.UPDATE_MESSAGE, -1);
            if (request == Constants.SERVICE_START) {
                if (playServicesAvailable) {
                    if (mGoogleApiClient.isConnected()) {
                        if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION)
                                != PackageManager.PERMISSION_GRANTED) {
                            Intent other = new Intent(Constants.BROADCAST_ACTION);
                            other.putExtra(Constants.MESSAGE, Constants.PERMISSION_REQUEST);
                            sendBroadcast(other);
                        } else {
                            LocationServices.FusedLocationApi.requestLocationUpdates(
                                    mGoogleApiClient, mLocationRequest, NotificationService.this
                            ).setResultCallback(
                                    new ResultCallback<Status>() {
                                        @Override
                                        public void onResult(@NonNull Status status) {
                                            mRequestingLocationUpdates = true;
                                            Log.d(Constants.TAG, "Started location updates!");
                                        }
                                    }
                            );
                        }

                        (new Thread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    Thread.sleep(30 * 1000);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                                if (mRequestingLocationUpdates) {

                                    Log.d(Constants.TAG, "Connection time out...");

                                    LocationServices.FusedLocationApi.removeLocationUpdates(
                                            mGoogleApiClient, NotificationService.this
                                    ).setResultCallback(
                                            new ResultCallback<Status>() {
                                                @Override
                                                public void onResult(@NonNull Status status) {
                                                    mRequestingLocationUpdates = false;
                                                    Log.d(Constants.TAG, "Removed location updates!");
                                                }
                                            }
                                    );

                                    alarmManager.set(
                                            AlarmManager.ELAPSED_REALTIME,
                                            SystemClock.elapsedRealtime() + delay,
                                            pendingIntent
                                    );
                                }
                            }
                        })).start();
                    }
                } else {
                    Log.d(Constants.TAG, "getLocation function to be called");
                    gps.getLocation();
                    (new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Thread.sleep(5 * 1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }


                            if (gps.mrequest) {
                                gps.stopUsingGPS();
                                Log.d(Constants.TAG, "Connection time out...");

                                alarmManager.set(
                                        AlarmManager.ELAPSED_REALTIME,
                                        SystemClock.elapsedRealtime() + delay,
                                        pendingIntent
                                );
                            }
                        }
                    })).start();
                }
            } else if (request == Constants.FIX_DELAY) {
                delay = intent.getIntExtra(Constants.UPDATE_DELAY, -1);
                Toast.makeText(NotificationService.this, String.valueOf(delay), Toast.LENGTH_SHORT).show();
            }
            Log.d(Constants.TAG, "Outside the function");
        }
    }
}
