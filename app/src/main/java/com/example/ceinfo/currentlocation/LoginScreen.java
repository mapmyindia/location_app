package com.example.ceinfo.currentlocation;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Login Screen for users
 * TODO handle errors in headers of back-end code
 */
public class LoginScreen extends AppCompatActivity {

    TextView mUsername, mPassword;
    Button mButton;

    /**
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //redirect to main activity if already logged in
        SharedPreferences sharedPreferences = getSharedPreferences(Constants.PREFS, MODE_PRIVATE);
        if (sharedPreferences.getString(CustomRequest.SESSION_COOKIE, null)!=null){
            Log.i(Constants.TAG, "Going directly to MainActivity");
            startActivity(new Intent(this, MainActivity.class));
        }

        setContentView(R.layout.activity_login_screen);

        //initialize variables
        mUsername = (TextView) findViewById(R.id.myEmail);
        mPassword = (TextView)findViewById(R.id.myPassword);
        mButton = (Button)findViewById(R.id.myButton);

        mButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.i(Constants.TAG, "Button on login screen clicked");
                        login();
                    }
                }
        );
    }

    /**
     * login the user with given user name and password
     */
    private void login() {

        Log.i(Constants.TAG, "Login attempt");
        final String username = mUsername.getText().toString();
        final String password = mPassword.getText().toString();
        //TODO validate username and password
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        CustomRequest customRequest = new CustomRequest(
                Request.Method.POST,
                Constants.base_url+"users/login",
                new Response.Listener<String>(){

                    /**
                     * if successful go to main activity screen
                     * otherwise display appropriate message
                     * @param response
                     */
                    @Override
                    public void onResponse(String response) {
                        Log.i(Constants.TAG, "Received response from login API");
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getBoolean("success")){
                                Log.i(Constants.TAG, "Login successful. Moving to MainActivity");
                                startActivity(new Intent(LoginScreen.this, MainActivity.class));
                            }else{
                                Log.w(Constants.TAG, "Wrong info provided");
                                Toast.makeText(LoginScreen.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            //not expected to reach here
                            Log.e(Constants.TAG, e.getMessage());
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener(){

                    /**
                     * display error
                     * @param error
                     */
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        //handle volley errors
                        if (error!=null){
                            Log.w(Constants.TAG, error.getMessage());
                        }else{
                            Log.w(Constants.TAG, "error came to be null");
                        }

                        if (error instanceof TimeoutError){
                            Log.w(Constants.TAG, "Connection time out");
                            Toast.makeText(LoginScreen.this, "Taking too long to connect", Toast.LENGTH_SHORT).show();
                        }else if (error instanceof NoConnectionError){
                            Log.w(Constants.TAG, "No Connection error");
                            Toast.makeText(LoginScreen.this, "Please check your internet connection", Toast.LENGTH_SHORT).show();
                        }else if (error instanceof AuthFailureError) {
                            Log.w(Constants.TAG, "Authorization failure");
                            Toast.makeText(LoginScreen.this, "Authorization failed", Toast.LENGTH_SHORT).show();
                        } else if (error instanceof ServerError) {
                            Log.e(Constants.TAG, "Server failure");
                            Toast.makeText(LoginScreen.this, "Server error occurred", Toast.LENGTH_SHORT).show();
                        } else if (error instanceof NetworkError) {
                            Log.w(Constants.TAG, "Network failure");
                            Toast.makeText(LoginScreen.this, "Network error occurred", Toast.LENGTH_SHORT).show();
                        } else if (error instanceof ParseError) {
                            Log.e(Constants.TAG, "Parse failure");
                            Toast.makeText(LoginScreen.this, "Parse error occurred", Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                this
        ){
            //send parameters through post method
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("username", username);
                params.put("password", password);
                return params;
            }
        };
        //add to request queue
        requestQueue.add(customRequest);
    }
}
