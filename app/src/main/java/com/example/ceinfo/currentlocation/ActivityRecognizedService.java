package com.example.ceinfo.currentlocation;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.DetectedActivity;

import java.util.List;

/**
 * Created by ce on 01-Jun-16.
 */
public class ActivityRecognizedService extends IntentService {

    int delay;
    public ActivityRecognizedService() {
        super("ActivityRecognizedService");
    }

    public ActivityRecognizedService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if(ActivityRecognitionResult.hasResult(intent)) {
            ActivityRecognitionResult result = ActivityRecognitionResult.extractResult(intent);
            handleDetectedActivities(result.getProbableActivities());
        }
    }

    private void handleDetectedActivities(List<DetectedActivity> probableActivities) {
        //TODO
        switch( probableActivities.get(0).getType() ) {
            case DetectedActivity.IN_VEHICLE: {
                Log.e( "ActivityRecogition", "In Vehicle: " + probableActivities.get(0).getConfidence() );
                delay= 15*1000;
                break;
            }
            case DetectedActivity.ON_BICYCLE: {
                Log.e( "ActivityRecogition", "On Bicycle: " + probableActivities.get(0).getConfidence() );
                delay= 45*1000;
                break;
            }
            case DetectedActivity.ON_FOOT: {
                Log.e( "ActivityRecogition", "On Foot: " + probableActivities.get(0).getConfidence() );
                delay= 90*1000;
                break;
            }
            case DetectedActivity.RUNNING: {
                Log.e( "ActivityRecogition", "Running: " + probableActivities.get(0).getConfidence() );
                delay= 60*1000;
                break;
            }
            case DetectedActivity.STILL: {
                Log.e( "ActivityRecogition", "Still: " + probableActivities.get(0).getConfidence() );
                delay= 10*60*1000;
                break;
            }
            case DetectedActivity.TILTING: {
                Log.e( "ActivityRecogition", "Tilting: " + probableActivities.get(0).getConfidence() );
                delay= 4*60*1000;
                break;
            }
            case DetectedActivity.WALKING: {
                Log.e( "ActivityRecogition", "Walking: " + probableActivities.get(0).getConfidence() );
                delay= 2*60*1000;
                break;
            }
            case DetectedActivity.UNKNOWN: {
                Log.e( "ActivityRecogition", "Unknown: " + probableActivities.get(0).getConfidence() );
                delay= 5*60*1000;
                break;
            }
        }
        Intent another = new Intent(Constants.UPDATE_BOADCAST);
        another.putExtra(Constants.UPDATE_MESSAGE, Constants.FIX_DELAY);
        another.putExtra(Constants.UPDATE_DELAY,delay);
        sendBroadcast(another);
    }
}

