package com.example.ceinfo.currentlocation;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by CEINFO on 01-06-2016.
 */
public class AlarmReceiver extends BroadcastReceiver{

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent other = new Intent(Constants.UPDATE_BOADCAST);
        other.putExtra(Constants.UPDATE_MESSAGE, Constants.SERVICE_START);
        context.sendBroadcast(other);
        Log.d(Constants.TAG, "Received from Alarm Manager");
    }
}
