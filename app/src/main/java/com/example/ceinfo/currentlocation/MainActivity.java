package com.example.ceinfo.currentlocation;


import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 */
public class MainActivity extends AppCompatActivity {

    boolean service_running;
    Button myButton;
    public String mUsername;
    AlarmManager alarmManager;
    PendingIntent pendingIntent;
    MyReceiver myReceiver;

    /**
     * customize a button depending upon whether service is running or not
     * created a receiver that receives broadcasts
     * initialised service running
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //initialize username
        //TODO username in table
        getUser();

        myButton = (Button) findViewById(R.id.myButton);

        //register with the given filter

        service_running = getSharedPreferences(Constants.PREFS, MODE_PRIVATE).getBoolean(Constants.SERVICE_STATUS, false);

        alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        Intent i = new Intent(this, AlarmManager.class);
        pendingIntent = PendingIntent.getBroadcast(this, 0, i, 0);

        customizeButton();

        int request = getIntent().getIntExtra(Constants.DISPLAY_ALERT, -1);
        if (request == Constants.PERMISSION_REQUEST){
            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    Constants.REQUEST_ACCESS_FINE_LOCATION);
        }else if (request == Constants.LOCATION_REQUEST){
            displayDialog();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        myReceiver = new MyReceiver();
        IntentFilter intentFilter = new IntentFilter(Constants.BROADCAST_ACTION);
        registerReceiver(myReceiver, intentFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        try{
            unregisterReceiver(myReceiver);
        }catch (IllegalArgumentException e){
            //TODO handle exception
        }
    }

    /**
     *
     */
    private void getUser() {

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        CustomRequest customRequest = new CustomRequest(
                Request.Method.GET,
                Constants.base_url + "users/user",
                new Response.Listener<String>() {
                    /**
                     * initialize username if successful
                     * @param response
                     */
                    @Override
                    public void onResponse(String response) {
                        Log.i(Constants.TAG, "Received response from user API");
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getBoolean("success")){
                                mUsername = jsonObject.getJSONObject("user").getString("username");
                            }else{
                                Log.e(Constants.TAG, "Unable to get user information");
                                Toast.makeText(MainActivity.this, "Unable to get user information", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            //never reached here
                            Log.e(Constants.TAG, e.getMessage());
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        //handle volley errors
                        if (error!=null){
                            Log.w(Constants.TAG, error.getMessage());
                        }else{
                            Log.w(Constants.TAG, "error came to be null");
                        }

                        if (error instanceof TimeoutError){
                            Log.w(Constants.TAG, "Connection time out");
                            Toast.makeText(MainActivity.this, "Taking too long to connect", Toast.LENGTH_SHORT).show();
                        }else if (error instanceof NoConnectionError){
                            Log.w(Constants.TAG, "No Connection error");
                            Toast.makeText(MainActivity.this, "Please check your internet connection", Toast.LENGTH_SHORT).show();
                        }else if (error instanceof AuthFailureError) {
                            Log.w(Constants.TAG, "Authorization failure");
                            Toast.makeText(MainActivity.this, "Authorization failed", Toast.LENGTH_SHORT).show();
                        } else if (error instanceof ServerError) {
                            Log.e(Constants.TAG, "Server failure");
                            Toast.makeText(MainActivity.this, "Server error occurred", Toast.LENGTH_SHORT).show();
                        } else if (error instanceof NetworkError) {
                            Log.w(Constants.TAG, "Network failure");
                            Toast.makeText(MainActivity.this, "Network error occurred", Toast.LENGTH_SHORT).show();
                        } else if (error instanceof ParseError) {
                            Log.e(Constants.TAG, "Parse failure");
                            Toast.makeText(MainActivity.this, "Parse error occurred", Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                this
        );
        requestQueue.add(customRequest);
    }

    /**
     * customizes button based on whether service running or not
     */
    public void customizeButton(){

        if (service_running){
            myButton.setText("Stop Service");
            myButton.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            stopNotificationService();
                        }
                    }
            );
        }else{
            myButton.setText("Start Service");
            myButton.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            startNotificationService();
                        }
                    }
            );
        }
    }

    /**
     * starts notification service
     */
    public void startNotificationService(){

        Intent startIntent = new Intent(MainActivity.this, NotificationService.class);
        startIntent.setAction(Constants.START_SERVICE);
        startIntent.putExtra(Constants.USERNAME, mUsername);
        startService(startIntent);
        Log.i(Constants.TAG, "Starting notification service");
        service_running = true;
        customizeButton();
    }

    /**
     * stops notification service
     */
    public void stopNotificationService(){
        Intent stopIntent = new Intent(MainActivity.this, NotificationService.class);
        stopIntent.setAction(Constants.STOP_SERVICE);
        startService(stopIntent);
        Log.i(Constants.TAG, "Stopping notification service");
        service_running = false;
        customizeButton();
    }

    /**
     * broadcast receiver
     */
    class MyReceiver extends BroadcastReceiver{


        @Override
        public void onReceive(Context context, Intent intent) {
            int request = intent.getIntExtra(Constants.MESSAGE, -1);
            if (request == Constants.PERMISSION_REQUEST){
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                        Constants.REQUEST_ACCESS_FINE_LOCATION);
                Log.i(Constants.TAG, "PERMISSION REQUEST");
            }else if (request == Constants.LOCATION_REQUEST){
                displayDialog();
                Log.i(Constants.TAG, "LOCATION REQUEST");
            }else if (request == Constants.EXIT_REQUEST){
                Log.i(Constants.TAG, "EXIT REQUEST");
                exit();
            }
        }
    }

    public void displayDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Settings required")
                .setMessage("Turn on high accuracy mode")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                stopNotificationService();
                exit();
            }
        });
        builder.create().show();
    }

    /**
     * exit the application-- not service
     */
    public void exit(){
        MainActivity.this.finish();
        moveTaskToBack(true);
        android.os.Process.killProcess(android.os.Process.myPid());
    }

    /**
     * start notification service when permission granted
     * close application otherwise
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.i(Constants.TAG, "Request handling");
        switch (requestCode){
            case Constants.REQUEST_ACCESS_FINE_LOCATION:
                if (grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    Log.i(Constants.TAG, "Request accepted");
                    startNotificationService();
                }else{
                    Log.i(Constants.TAG, "Request denied");
                    stopNotificationService();
                    exit();
                }
                break;
            default:stopNotificationService();
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.i(Constants.TAG, "Menu create");
        menu.add(1,1,1,"Log Out");
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == 1){
            Log.i(Constants.TAG, "Logout pressed");
            logout();
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * logout of given application
     */
    private void logout() {

        Log.i(Constants.TAG, "Logout called");
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        CustomRequest customRequest = new CustomRequest(
                Request.Method.GET,
                Constants.base_url+"users/logout",
                new Response.Listener<String>(){

                    /**
                     * logout if successful
                     * @param response
                     */
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getBoolean("success")){

                                Log.i(Constants.TAG, "Response successful");
                                SharedPreferences.Editor editor = getSharedPreferences(Constants.PREFS, MODE_PRIVATE).edit();
                                editor.putString(CustomRequest.SESSION_COOKIE, null);
                                editor.commit();

                                startActivity(new Intent(MainActivity.this, LoginScreen.class));
                            }else{
                                Log.e(Constants.TAG, "Logout does not give this output");
                            }
                        } catch (JSONException e) {
                            Log.i(Constants.TAG, "Shouldn't reach here P:)");
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener(){

                    @Override
                    public void onErrorResponse(VolleyError error) {

                        //handle volley errors
                        if (error!=null){
                            Log.w(Constants.TAG, error.getMessage());
                        }else{
                            Log.w(Constants.TAG, "error came to be null");
                        }

                        if (error instanceof TimeoutError){
                            Log.w(Constants.TAG, "Connection time out");
                            Toast.makeText(MainActivity.this, "Taking too long to connect", Toast.LENGTH_SHORT).show();
                        }else if (error instanceof NoConnectionError){
                            Log.w(Constants.TAG, "No Connection error");
                            Toast.makeText(MainActivity.this, "Please check your internet connection", Toast.LENGTH_SHORT).show();
                        }else if (error instanceof AuthFailureError) {
                            Log.w(Constants.TAG, "Authorization failure");
                            Toast.makeText(MainActivity.this, "Authorization failed", Toast.LENGTH_SHORT).show();
                        } else if (error instanceof ServerError) {
                            Log.e(Constants.TAG, "Server failure");
                            Toast.makeText(MainActivity.this, "Server error occurred", Toast.LENGTH_SHORT).show();
                        } else if (error instanceof NetworkError) {
                            Log.w(Constants.TAG, "Network failure");
                            Toast.makeText(MainActivity.this, "Network error occurred", Toast.LENGTH_SHORT).show();
                        } else if (error instanceof ParseError) {
                            Log.e(Constants.TAG, "Parse failure");
                            Toast.makeText(MainActivity.this, "Parse error occurred", Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                this
        );
        requestQueue.add(customRequest);
    }

    @Override
    public void onBackPressed() {

        Log.i(Constants.TAG, "back button pressed");
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
