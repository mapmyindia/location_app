package com.example.ceinfo.currentlocation;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.location.Location;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by ce on 30-May-16.
 */
public class DBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "MyDBName.db";
    public String mUsername;

    /**
     * call upon a database
     * set the value of user name
     * @param context
     * @param username
     */
    public DBHelper(Context context, String username) {
        super(context, DATABASE_NAME , null, 1);
        mUsername = username;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //TODO
        db.execSQL(
                "create table locations " +
                        "(id INTEGER primary key AUTOINCREMENT, longitude DOUBLE,latitude DOUBLE,altitude DOUBLE,accuracy FLOAT,bearing FLOAT," +
                        "speed FLOAT,status INTEGER,provider VARCHAR(8),time BIGINT)"

        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS locations");
        onCreate(db);
    }

    public void addLocation(Location location) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("longitude", location.getLongitude());
        contentValues.put("latitude", location.getLatitude());
        contentValues.put("altitude", location.getAltitude());
        contentValues.put("accuracy", location.getAccuracy());
        contentValues.put("bearing", location.getBearing());
        contentValues.put("speed", location.getSpeed());
        contentValues.put("status", 0);
        contentValues.put("provider",location.getProvider());
        contentValues.put("time", location.getTime());
        db.insert("locations", null, contentValues);
        db.close();
    }

    public void updateStatus(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("update locations set status = 1 where id ="+id+"");
        db.close();
    }

    /**
     * output - all locations data, only whose status is 0
     * @return
     */
    public ArrayList<String[]> getAllLocations() {
        ArrayList<String[]> array_list = new ArrayList<String[]>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from locations where status="+0+"", null );

        Log.d(Constants.TAG, "Queried all locations");

        res.moveToFirst();

        while(res.isAfterLast() == false){
            String[] data = new String[9];
            data[0]= res.getString(res.getColumnIndex("longitude"));
            data[1]= res.getString(res.getColumnIndex("latitude"));
            data[2]= res.getString(res.getColumnIndex("altitude"));
            data[3]= res.getString(res.getColumnIndex("accuracy"));
            data[4]= res.getString(res.getColumnIndex("bearing"));
            data[5]= res.getString(res.getColumnIndex("speed"));
            data[6]= res.getString(res.getColumnIndex("provider"));
            data[7]= res.getString(res.getColumnIndex("time"));
            data[8] = res.getString(res.getColumnIndex("id"));
            array_list.add(data);
            res.moveToNext();
        }
        db.close();
        return array_list;
    }

    /**
     * delete location corresponding to specific id
     * @param id
     * @return
     */
    public Integer clear(Integer id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("contacts",
                "id = ? ",
                new String[] { Integer.toString(id) });
    }
}
